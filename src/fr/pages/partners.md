---
layout: 'layouts/simple-page.njk'
title: Nos partenaires
pageName: partners
permalink: /{{ locale }}/{{ pageName }}/
---

<h1 class="section-title"> {{ title }}</h1>

<p class="lead-text">Les services fournis par Altertek sont rendus possibles avec l'aide de différents contributeurs, organisations et partenaires.
Nous proposons également de l'aide aux associations ayant des valeurs communes aux nôtres.
Dans cette page, notre définition de "sponsor" comprend toute aide financière, matérielle ou service offert.</p>

<hr/>

## Nous sommes membre de
- [Le collectif CHATONS](https://www.chatons.org/)
- [L'April](https://www.april.org/)
- [Les assembleurs](https://les.assembleurs.co/)

## Nous soutenons
- [World Cleanup day France](https://www.worldcleanupday.fr): Nous y assurons la gestion des services numériques
- [Zero Waste France](https://www.zerowastefrance.org): Aide à la démocratisation de Nextcloud dans l'association
- [Alternatiba](https://alternatiba.eu): Aide sur l'infrastructure pendant les camps climat

## Nous sommes sponsorisés par
[<img src="../../assets/images/references/hexatech.svg" height="100">](https://hexatech.eu)
Principal Sponsor  
  
- [Updown](https://updown.io): Mise à disposition gratuite de [l'offre de monitoring](https://updown.io/#pricing)
- [GitHub](https://github.com): Nous utilisons un [compte à but non lucratif GitHub](https://github.com/nonprofit) pour notre compte d'organisation GitHub
