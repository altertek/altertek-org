---
layout: 'layouts/simple-page.njk'
title: Credits
pageName: credits
permalink: /{{ locale }}/{{ pageName }}/
---

# {{ title }}

Conception: Altertek

Content under Creative Commons BY-SA.

List of open source projects used for this website:
- [Eleventy](https://www.11ty.dev/)
- [HTMLminifier](https://github.com/kangax/html-minifier)
- [PurifyCSS](https://github.com/purifycss/purifycss)
- [Bootstrap](https://getbootstrap.com)
- [Jquery](https://jquery.com/)
- [Fork-Awesome](https://forkaweso.me/Fork-Awesome/)

Accessibility: Valid (see [Wave test](https://wave.webaim.org/report#/https://altertek.org/))

Wanna contribute? See the [contribute page](/{{ locale }}/contribute)
